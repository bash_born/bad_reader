#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[]){

	srand(time(NULL));

	FILE *in;

	int cycles = atoi(argv[2]), j = 1;

	char ch;

	char *banner = 
" ▄▄▄▄    ▄▄▄      ▓█████▄     ██▀███  ▓█████ ▄▄▄      ▓█████▄ ▓█████  ██▀███  \n"
"▓█████▄ ▒████▄    ▒██▀ ██▌   ▓██ ▒ ██▒▓█   ▀▒████▄    ▒██▀ ██▌▓█   ▀ ▓██ ▒ ██▒\n"
"▒██▒ ▄██▒██  ▀█▄  ░██   █▌   ▓██ ░▄█ ▒▒███  ▒██  ▀█▄  ░██   █▌▒███   ▓██ ░▄█ ▒\n"
"▒██░█▀  ░██▄▄▄▄██ ░▓█▄   ▌   ▒██▀▀█▄  ▒▓█  ▄░██▄▄▄▄██ ░▓█▄   ▌▒▓█  ▄ ▒██▀▀█▄  \n"
"░▓█  ▀█▓ ▓█   ▓██▒░▒████▓    ░██▓ ▒██▒░▒████▒▓█   ▓██▒░▒████▓ ░▒████▒░██▓ ▒██▒\n"
"░▒▓███▀▒ ▒▒   ▓▒█░ ▒▒▓  ▒    ░ ▒▓ ░▒▓░░░ ▒░ ░▒▒   ▓▒█░ ▒▒▓  ▒ ░░ ▒░ ░░ ▒▓ ░▒▓░\n"
"▒░▒   ░   ▒   ▒▒ ░ ░ ▒  ▒      ░▒ ░ ▒░ ░ ░  ░ ▒   ▒▒ ░ ░ ▒  ▒  ░ ░  ░  ░▒ ░ ▒░\n"
" ░    ░   ░   ▒    ░ ░  ░      ░░   ░    ░    ░   ▒    ░ ░  ░    ░     ░░   ░ \n"
" ░            ░  ░   ░          ░        ░  ░     ░  ░   ░       ░  ░   ░     \n"
"      ░            ░                                   ░                      \n\n";

	char *help = "\n"
	"Usage:\n\n"
	"\t./bad_reader <file to damage> <number of cycles>\n\n";

	if(argc != 3){

		puts(help);
		
		exit(0);
	}

	puts(banner);

	printf("File: %s\nNumber of cycles:%s\n[*] Starting...\n", argv[1], argv[2]);

	if(!(in = fopen(argv[1], "r+w"))){

		perror("");

		return -1;

	}

	for(j; j <= cycles; j++){

		fseek(in, 0, SEEK_SET);		

		while(!(feof(in))){

			ch = fgetc(in);
			
			ch = rand();	

			fprintf(in, "%c", ch);
					
		}
	
		printf("[+] Cycle number %d passed\n", j);

	}
	
	printf("[+] Program completed !\n[*] I STRONGLY recommend restarting your computer or rewriting the contents or RAM\n[*] Have a nice day, Master ^-^\n");

	return 0;

}
